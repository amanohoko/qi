class Registry(object):
    def __init__(self, target_class):
        self._trg = target_class

        self._reg = {}

    target_type = property(lambda self: self._trg)

    ############################################################################

    def __call__(self, *args, **kwargs):
        def do_apply(handler, *narrow, **opts):
            if type(narrow) in (tuple, set, frozenset, list):
                if len(narrow)==1:
                    narrow = narrow[0]
            
            if narrow not in self._reg:
                resp = {
                    'name':   narrow,
                    'class':  handler,
                    'schema': {},
                }

                for field in ('title', 'icon'):
                    if field in opts:
                        resp[field] = opts[field]

                        del opts[field]

                resp['config'] = opts

                self._reg[narrow] = resp

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def mapping(self, *args, **kwargs):
        def do_apply(handler, ontology, narrow, *args, **opts):
            if narrow in self._reg:
                if ontology not in self._reg[narrow]['schema']:
                    self._reg[narrow]['schema'][ontology] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def all(self):
        resp = []

        for key in self._reg:
            resp.append(self._reg[key])

        return resp

    #***************************************************************************

    def resolve(self, narrow, *args, **kwargs):
        if narrow.endswith('.oauth2'):
            narrow = narrow.replace('.oauth2', '')
        
        resp = []
        
        for entry in self._reg.values():
            if narrow==entry['name']:
                resp += [entry]
        
        return resp

    #***************************************************************************

    def get_class(self, *narrow):
        resp = self.resolve(*narrow)
        
        if len(resp):
            return resp[0]['class']
        else:
            return None

    #***************************************************************************

    def get_instance(self, narrow, *args, **kwargs):
        hnd = self.get_class(narrow)

        if hnd is not None:
            return hnd(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def get_meta(self, *narrow):
        resp = self.resolve(*narrow)
        
        if len(resp):
            return resp[0]['schema'].keys()
        else:
            return None

    #***************************************************************************

    def get_mapping(self, narrow, ontology, *args, **kwargs):
        resp = self.resolve(narrow)
        
        if len(resp):
            if ontology in resp[0]['schema']:
                hnd = resp[0]['schema'][ontology]
                
                inst = hnd(self, ontology, *args, **kwargs)
                
                return inst
        
        return None

################################################################################

class Ontology2use(object):
    def __init__(self, target, *args, **kwargs):
        self._trg  = target

        if hasattr(self, 'prepare'):
            if callable(self.prepare):
                self.prepare(*args, **kwargs)

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    target   = property(lambda self: self._trg)

#*******************************************************************************

class Api2use(object):
    def __init__(self, target, *args, **kwargs):
        self._trg  = target

        if hasattr(self, 'prepare'):
            if callable(self.prepare):
                self.prepare(*args, **kwargs)

        for key in self.register.get_meta(self.provider):
            hnd = self.register.get_mapping(self.provider, key)
            
            setattr(self, key, hnd)

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    target   = property(lambda self: self._trg)

    provider = property(lambda self: self.target.provider)

    #***************************************************************************

    class Manager(object):
        def __init__(self, parent, alias, *args, **kwargs):
            self._trg = parent
            self._key = alias
            
            if hasattr(self, 'initialize'):
                if callable(self.initialize):
                    self.initialize(*args, **kwargs)
        
        target = property(lambda self: self._trg)
        alias  = property(lambda self: self._key)
        
        def wrap(self, narrow, payload):
            pass

    #***************************************************************************

    class Resource(object):
        def __init__(self, parent, ontology, narrow, payload, *args, **kwargs):
            self._trg = parent
            self._ont = ontology

            self._nrw = narrow
            self._raw = payload
            
            if hasattr(self, 'initialize'):
                if callable(self.initialize):
                    self.initialize(*args, **kwargs)
        
        target   = property(lambda self: self._trg)
        ontology = property(lambda self: self._ont)

        narrow   = property(lambda self: self._nrw)
        payload  = property(lambda self: self._raw)

################################################################################

class Social2use(Api2use):
    provider = property(lambda self: self.target.provider)

    def prepare(self, *args, **kwargs):
        RESERVED_FIELDs = ('key', 'secret')

        from code2use.webapp import settings

        self.creds  = {}

        for x in RESERVED_FIELDs:
            prop = '_'.join([
                'SOCIAL', 'AUTH',
                self.provider.upper().replace('-', '_'),
                x.upper(),
            ])

            if hasattr(settings, prop):
                self.creds['api_' + x] = getattr(settings, prop)

        self.config = {}

        prefix = '_'.join([
            'SOCIAL', 'AUTH',
            self.provider.upper().replace('-', '_'),
            '',
        ])

        for prop in dir(settings):
            if prop.startswith(prefix):
                key = prop.replace(prefix, '').lower()

                if key not in RESERVED_FIELDs:
                    self.config[key] = getattr(settings, prop)

        self.vault  = self.target.access_token
        
        if type(self.vault) not in [dict]:
            self.vault = {
                'access_token': self.vault,
            }

#*******************************************************************************

class Cloud2use(Api2use):
    def prepare(self, *args, **kwargs):
        self.creds  = self.target.creds
        self.config = self.target.config
        self.vault  = self.target.vault

#*******************************************************************************

class Endpoint2use(Api2use):
    alias = property(lambda self: self.target.alias)

    def prepare(self, *args, **kwargs):
        self.creds  = self.target.creds
        self.config = self.target.config
        self.vault  = self.target.vault

################################################################################

for target in (Ontology2use, Social2use, Cloud2use, Endpoint2use):
    obj = Registry(target)

    setattr(target, 'register', obj)

