#-*- coding: utf-8 -*-

from django.http import HttpResponseRedirect, HttpResponseNotFound, Http404
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.messages.api import get_messages

from django.conf import urls as dj_urls

#*******************************************************************************

from subdomains.utils import reverse

from social_auth import __version__ as version

################################################################################

from code2use.core.abstract import *

#*******************************************************************************

class WebDomain(object):
    def __init__(self, ns, fqdn):
        self._ns   = ns
        self._fqdn = fqdn

        self._lst  = []

        from code2use.webapp import settings

        settings.SUBDOMAIN_URLCONFS[self._fqdn] = self.namespace

    #alias     = property(lambda self: self._fqdn)
    namespace = property(lambda self: self._ns)

    def route(self, *args, **kwargs):
        def do_apply(handler, pattern, *params, **opts):
            params = [x for x in params]

            if not hasattr(handler, 'VERBs'):
                params += [handler]

                handler = WebView

            handler = handler(self, pattern, *params, **opts)

            self._lst.append(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    @property
    def urlpatters(self):
        from django.conf.urls import patterns, url

        return patterns(self.namespace, *[
            url(res.pattern, res.target, name=res.alias)
            for res in self._lst
        ])

#*******************************************************************************

class WebContext(object):
    def __init__(self, request, *args, **kwargs):
        self._req     = request
        self._resp    = None

        self._args    = args
        self._opts    = kwargs

        self.template = None

        self._social  = {}

        for soc in self.user.social_auth.all():
            for prvd in [Social2use]:
                hnd = prvd.register.get_class(soc.provider)

                if hnd:
                    self._social[soc.provider] = hnd(soc)

    ############################################################################

    request    = property(lambda self: self._req)
    response   = property(lambda self: self._resp)

    arguments  = property(lambda self: self._args)
    parameters = property(lambda self: self._opts)

    social     = property(lambda self: self._social)

    #***************************************************************************

    method     = property(lambda self: self.request.method)

    GET        = property(lambda self: self.request.GET)
    POST       = property(lambda self: self.request.POST)

    #***************************************************************************

    user       = property(lambda self: self.request.user)
    host       = property(lambda self: self.request.host)
    subdomain  = property(lambda self: self.request.subdomain)

    ############################################################################

    def redirect(self, *args, **kwargs):
        return HttpResponseRedirect(*args, **kwargs)

    def reverse(self, *args, **kwargs):
        return reverse(*args, **kwargs)

    def render_tpl(self, template_path, **config):
        self._resp = render_to_response(template_path, config, RequestContext(self.request))

#*******************************************************************************

class WebResource(object):
    VERBs = ['GET', 'POST']

    def __init__(self, domain, pattern, *args, **kwargs):
        self._fqdn = domain
        self._ptn  = pattern

        self._cfg = dict(
            strategy='anon',
            csrf=True,
        )

        for key in self._cfg:
            if key in kwargs:
                self._cfg[key] = kwargs[key]

                del kwargs[key]

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    domain  = property(lambda self: self._fqdn)
    pattern = property(lambda self: self._ptn)
    config  = property(lambda self: self._cfg)

    namespace = property(lambda self: self.domain.namespace)
    ns_prefix  = property(lambda self: self.namespace + '.')

    @property
    def target(self):
        resp = self

        if self.config['strategy']=='login':
            resp = login_required(resp)

        if not self.config['csrf']:
            resp = csrf_exempt(resp)

        return resp

    def __call__(self, request, *args, **kwargs):
        cnt = WebContext(request, *args, **kwargs)

        if hasattr(self, 'invoke'):
            if callable(self.invoke):
                resp = self.invoke(cnt)

                if type(resp) in [dict] and len(cnt.template or ''):
                    cnt.render_tpl(cnt.template, **resp)

                    resp = None

        if resp is None:
            if cnt.response is not None:
                resp = cnt.response

        if resp is not None:
            return resp
        else:
            return HttpResponseNotFound()

################################################################################

class WebView(WebResource):
    def initialize(self, handler, *args, **kwargs):
        self._hnd = handler

        self._path = self.handler.__module__ + '.' + self.handler.__name__

        self._path = self._path.replace(self.ns_prefix, '')

        while '.' in self._path:
            self._path = self._path.replace('.', '__')

    alias     = property(lambda self: self._path)
    handler   = property(lambda self: self._hnd)

    def invoke(self, context):
        return self.handler(context, *context.arguments, **context.parameters)

#*******************************************************************************

class WebAPI(WebResource):
    alias   = property(lambda self: self._fqdn)

    def initialize(self, *args, **kwargs):
        pass

    def invoke(self, context):
        hnd = getattr(self, context.request.method, None)

        if callable(hnd):
            return hnd(context)
        else:
            raise NotImplemented()
