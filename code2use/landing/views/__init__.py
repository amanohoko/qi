#-*- coding: utf-8 -*-

from lobby.website.shortcuts import *

################################################################################

@fqdn.route(r'^$')
def homepage(context):
    context.template = 'www/views/homepage.html'

    return {'version': version}

################################################################################

urlpatterns = fqdn.urlpatters
