def HTML5_Menus(request):
    return {
        'user': request.user,
        'sections': [
            dict(
                name=x, title=x.capitalize(),
                template='%s/menu.html' % x,
                icon={
                    'connect':  'signal',
                    'console':  'terminal',
                    'hub':      'plug',

                    'graph':    'bar-chart-o',
                    'semantic': 'sitemap',
                    'data':     'table',

                    'dev':      'code',
                    'ops':      'server',
                    'cloud':    'cloud',
                }.get(x, 'home'),
                skin={
                    'connect':  'red',
                    'console':  'grey',
                    'hub':      'purple',
                    'graph':    'blue',
                    'semantic': 'green',
                    'data':     'yellow',
                    'dev':      'orange',
                    'ops':      'o',
                    'cloud':    'yellow',
                }.get(x, 'green'),
            )
            for x in (
                'connect', 'console', 'hub',
                'graph', 'data', 'semantic',
                'dev', 'ops', 'cloud',
                #'code', 'hosting', 'platform',
            )
        ],
    }
