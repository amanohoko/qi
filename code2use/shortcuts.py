#-*- coding: utf-8 -*-

from __future__ import absolute_import

#*******************************************************************************

import os, sys, re

#*******************************************************************************

import yaml
import simplejson as json

#*******************************************************************************

from celery import shared_task

################################################################################

from code2use.core.routing import WebDomain

#*******************************************************************************

from code2use.webapp import settings

from code2use.webapp.app.models import *

################################################################################

from django                         import forms
from django.db                      import models
from django.db.models               import Q
from django.http                    import HttpResponseRedirect
from django.template                import RequestContext
from django.shortcuts               import render_to_response, redirect
from django.views.decorators.csrf   import csrf_exempt

from django.contrib.auth            import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.messages.api    import get_messages

from social_auth import __version__ as version

#*******************************************************************************

from django.conf import urls as dj_urls

from subdomains.utils import reverse

################################################################################

import mongoengine as mongo

for alias,config in settings.MONGO_DBs.iteritems():
    config['alias'] = alias

    mongo.connect(**config)

mongo.TextField = mongo.StringField

################################################################################

import code2use.ontologies

import code2use.backends

################################################################################

from code2use.webapp.celery import app as workers
