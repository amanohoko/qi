from django.contrib import admin

from code2use.webapp.app.models import *

################################################################################

class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_active', 'is_staff', 'location', 'website', 'date_joined')
    list_filter  = ('is_active', 'is_staff', 'date_joined')

admin.site.register(CustomUser, CustomUserAdmin)
