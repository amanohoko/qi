from django import forms

from .models import *

class ProfileForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = (
            'email',
            'first_name', 'last_name',
            'location', 'website',
        )

class SettingsForm(forms.Form):
    foobar = forms.CharField(max_length=128)
