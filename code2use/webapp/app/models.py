# Define a custom User class to work with django-social-auth
from django.contrib.auth.models import AbstractUser

from django.db import models

class CustomUser(AbstractUser):
    location = models.CharField(max_length=256, blank=True)
    website  = models.CharField(max_length=256, blank=True)

    mugshot  = property(lambda self: 'mugshot/%(username)s.jpg' % self.__dict__)
