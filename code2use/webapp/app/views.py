import json

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login
from django.template import RequestContext

from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.google import GooglePlusAuth
from social.backends.utils import load_backends
from social.apps.django_app.utils import psa

from code2use.webapp.app.decorators import render_to

from .models import *
from .forms import *

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/')


def context(**extra):
    return dict({
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
        'plus_scope': ' '.join(GooglePlusAuth.DEFAULT_SCOPE),
        'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS)
    }, **extra)


@render_to('connect/homepage.html')
def home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        print settings.SUBDOMAIN_URLCONFS

        return context()
    else:
        return redirect('done')

@login_required
@render_to('connect/profile/view.html')
def profile_form(request, acte='view'):
    if acte not in ('view', 'edit'):
        raise Http404("Unknown acte : %s !" % acte)

    resp = {}

    if acte=='edit':
        form = ProfileForm(instance=request.user)

        if request.method=='POST':
            form = ProfileForm(request.POST, instance=request.user)

            if form.is_valid():
                obj = form.save(commit=False)

                obj.save()

        resp['form'] = form

    return render_to_response('connect/profile/%s.html' % acte, context(**resp), RequestContext(request))

@login_required
@render_to('connect/settings.html')
def settings_form(request):
    form = SettingsForm()

    if request.method=='POST':
        form = SettingsForm(request.POST)

        if form.is_valid():
            cfg = form.cleaned_data

            print cfg

    return context(
        form = form,
    )

def accounts(request):
    cnt = context(

    )

    if request.user.is_authenticated():
        return render_to_response('connect/accounts/logged-in.html', cnt, RequestContext(request))
    else:
        return render_to_response('connect/accounts/anonymous.html', cnt, RequestContext(request))

@render_to('connect/homepage.html')
def validation_sent(request):
    return context(
        validation_sent=True,
        email=request.session.get('email_validation_address')
    )


@render_to('connect/homepage.html')
def require_email(request):
    backend = request.session['partial_pipeline']['backend']
    return context(email_required=True, backend=backend)


@psa('social:complete')
def ajax_auth(request, backend):
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')
