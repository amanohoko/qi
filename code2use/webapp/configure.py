TIME_ZONE = 'Africa/Casablanca'
LANGUAGE_CODE = 'en-us'

ADMINS = (
    # ('Your Name', 'your_email@code2use.webapp.com'),
)

SECRET_KEY = '#$5btppqih8=%ae^#&amp;7en#kyi!vh%he9rg=ed#hm6fnw9^=umc'
SESSION_COOKIE_DOMAIN = '.scubadev.com'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'subdomains',
    'django_extensions',
    'django_cron',

    'social.apps.django_app.default',

    'code2use.webapp.app',

    'lobby.hub',
    'lobby.console',

    'vortex.data',
    'vortex.semantic',
    'vortex.graph',

    'reflector.dev',
    'reflector.ops',
    'reflector.cloud',

    'lobby.website',
)

SUBDOMAIN_URLCONFS = {
    #None:      'lobby.dummy.views',
    'connect': 'code2use.webapp.urls',
}

AUTHENTICATION_BACKENDS = (
    #'social.backends.persona.PersonaAuth',
    'social.backends.google.GoogleOAuth2',
    #'social.backends.yahoo.YahooOAuth',

    #'social.backends.amazon.AmazonOAuth2',
    #'social.backends.digitalocean.DigitalOceanOAuth2',

    'social.backends.launchpad.LaunchpadOpenId',

    'social.backends.github.GithubOAuth2',
    'social.backends.bitbucket.BitbucketOAuth2',

    #'social.backends.slack.SlackOAuth',
    #'social.backends.docker.DockerOAuth2',

    'social.backends.trello.TrelloOAuth',
    'social.backends.pocket.PocketAuth',

    'social.backends.facebook.FacebookOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.foursquare.FoursquareOAuth2',
    'social.backends.linkedin.LinkedinOAuth2',

    #'social.backends.flickr.FlickrOAuth',
    #'social.backends.dailymotion.DailymotionOAuth2',

    'social.backends.dropbox.DropboxOAuth2',
    #'social.backends.box.BoxOAuth2',

    #'social.backends.angel.AngelOAuth2',
    #'social.backends.aol.AOLOpenId',
    #'social.backends.appsfuel.AppsfuelOAuth2',
    #'social.backends.beats.BeatsOAuth2',
    #'social.backends.behance.BehanceOAuth2',
    #'social.backends.belgiumeid.BelgiumEIDOpenId',
    #'social.backends.clef.ClefOAuth2',
    #'social.backends.coinbase.CoinbaseOAuth2',
    #'social.backends.coursera.CourseraOAuth2',
    #'social.backends.dailymotion.DailymotionOAuth2',
    #'social.backends.disqus.DisqusOAuth2',
    #'social.backends.douban.DoubanOAuth2',
    #'social.backends.dropbox.DropboxOAuth',
    #'social.backends.eveonline.EVEOnlineOAuth2',
    #'social.backends.evernote.EvernoteSandboxOAuth',
    #'social.backends.fedora.FedoraOpenId',
    #'social.backends.fitbit.FitbitOAuth',
    #'social.backends.google.GoogleOAuth',
    #'social.backends.google.GoogleOpenId',
    #'social.backends.google.GooglePlusAuth',
    #'social.backends.google.GoogleOpenIdConnect',
    #'social.backends.instagram.InstagramOAuth2',
    #'social.backends.jawbone.JawboneOAuth2',
    #'social.backends.kakao.KakaoOAuth2',
    #'social.backends.linkedin.LinkedinOAuth',
    #'social.backends.live.LiveOAuth2',
    #'social.backends.livejournal.LiveJournalOpenId',
    #'social.backends.mailru.MailruOAuth2',
    #'social.backends.mendeley.MendeleyOAuth',
    #'social.backends.mendeley.MendeleyOAuth2',
    #'social.backends.mineid.MineIDOAuth2',
    #'social.backends.mixcloud.MixcloudOAuth2',
    #'social.backends.nationbuilder.NationBuilderOAuth2',
    #'social.backends.odnoklassniki.OdnoklassnikiOAuth2',
    #'social.backends.openstreetmap.OpenStreetMapOAuth',
    #'social.backends.podio.PodioOAuth2',
    #'social.backends.rdio.RdioOAuth1',
    #'social.backends.rdio.RdioOAuth2',
    #'social.backends.readability.ReadabilityOAuth',
    #'social.backends.reddit.RedditOAuth2',
    #'social.backends.runkeeper.RunKeeperOAuth2',
    #'social.backends.skyrock.SkyrockOAuth',
    #'social.backends.soundcloud.SoundcloudOAuth2',
    #'social.backends.spotify.SpotifyOAuth2',
    #'social.backends.stackoverflow.StackoverflowOAuth2',
    #'social.backends.steam.SteamOpenId',
    #'social.backends.stocktwits.StocktwitsOAuth2',
    #'social.backends.stripe.StripeOAuth2',
    #'social.backends.suse.OpenSUSEOpenId',
    #'social.backends.thisismyjam.ThisIsMyJamOAuth1',
    #'social.backends.tripit.TripItOAuth',
    #'social.backends.tumblr.TumblrOAuth',
    #'social.backends.twilio.TwilioAuth',
    #'social.backends.vk.VKOAuth2',
    #'social.backends.weibo.WeiboOAuth2',
    #'social.backends.wunderlist.WunderlistOAuth2',
    #'social.backends.xing.XingOAuth',
    #'social.backends.yahoo.YahooOpenId',
    #'social.backends.yammer.YammerOAuth2',
    #'social.backends.yandex.YandexOAuth2',
    #'social.backends.vimeo.VimeoOAuth1',
    #'social.backends.lastfm.LastFmAuth',
    #'social.backends.moves.MovesOAuth2',
    #'social.backends.vend.VendOAuth2',
    'social.backends.email.EmailAuth',
    'social.backends.username.UsernameAuth',
    'django.contrib.auth.backends.ModelBackend',
)
