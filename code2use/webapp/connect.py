SOCIAL_AUTH_GOOGLE_OAUTH2_KEY       = '129394993821-uslju80m21klshig8ron0r9td67kij0n.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET    = 'dafhcurrsvGnxRIhqG1elboH'

SOCIAL_AUTH_GOOGLE_PLUS_AUTH_EXTRA_ARGUMENTS = {
      'access_type': 'offline'
}

SOCIAL_AUTH_GOOGLE_OAUTH2_USE_DEPRECATED_API = True

#SOCIAL_AUTH_AMAZON_KEY              = '...'
#SOCIAL_AUTH_AMAZON_SECRET           = '...'

#SOCIAL_AUTH_AZUREAD_OAUTH2_KEY = ''
#SOCIAL_AUTH_AZUREAD_OAUTH2_SECRET = ''

#*******************************************************************************

#SOCIAL_AUTH_DIGITALOCEAN_KEY        = ''
#SOCIAL_AUTH_DIGITALOCEAN_SECRET     = ''
#SOCIAL_AUTH_DIGITALOCEAN_AUTH_EXTRA_ARGUMENTS = {'scope': 'read write'}

#*******************************************************************************

SOCIAL_AUTH_GITHUB_KEY              = 'd5d4c0aac0b6ba60e8fa'
SOCIAL_AUTH_GITHUB_SECRET           = '1a02a07b7df1f08ecd85143e551630e38409e480'

SOCIAL_AUTH_BITBUCKET_OAUTH2_KEY    = 'QePr3mHk3UkMwUJ4Ee'
SOCIAL_AUTH_BITBUCKET_OAUTH2_SECRET = 'bDm97R5rwsQD88nVExm4us2tpqz8rByQ'

#*******************************************************************************

SOCIAL_AUTH_SLACK_KEY               = '4932747637.8843610483'
SOCIAL_AUTH_SLACK_SECRET            = '065438444eda6bcd248ee558c1004af4'

SOCIAL_AUTH_DOCKER_KEY = ''
SOCIAL_AUTH_DOCKER_SECRET = ''

#*******************************************************************************

SOCIAL_AUTH_TRELLO_KEY              = 'c65e3f78706e63f4efe83a12a9329ad5'
SOCIAL_AUTH_TRELLO_SECRET           = '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01'
SOCIAL_AUTH_TRELLO_APP_NAME         = 'Scuba Diving'

SOCIAL_AUTH_POCKET_CONSUMER_KEY     = '44413-714984d478b597cb5833c741'

#*******************************************************************************

SOCIAL_AUTH_FACEBOOK_KEY            = '859216634144977'
SOCIAL_AUTH_FACEBOOK_SECRET         = '22f891ba51b8ac95b6a17cf406159c1f'

SOCIAL_AUTH_TWITTER_KEY             = 'szd6KQSnupSEU0c9kN0cj5Y4s'
SOCIAL_AUTH_TWITTER_SECRET          = 'og2nl2EVJXNs3aksTfXZUxy7jXCMd1Yjk54KwpfDq7zucM3lCs'

SOCIAL_AUTH_FOURSQUARE_KEY          = '2AWMWVZ33F4OH32XDZQCL2JMIN3W34JP5JNHFHAH2MQIHWEK'
SOCIAL_AUTH_FOURSQUARE_SECRET       = 'H3MJZ2TCGSMZEPS3YQCPEI3MZQYM3A5C1ZJ00UFA0QU2PTOF'

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY     = '77hn8oodp0qfhl'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET  = 'W48Bhb2zXQoaK0oq'

#*******************************************************************************

#*******************************************************************************

SOCIAL_AUTH_DROPBOX_OAUTH2_KEY      = 'cwa0bh213w323cd'
SOCIAL_AUTH_DROPBOX_OAUTH2_SECRET   = 'mdelltidadvle8e'

################################################################################

import os

from urlparse import urlparse

################################################################################

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'files/main.sqlite',
    }
}

MONGO_DBs = {
    'local': {
        'host': 'mongodb://localhost/%s' % os.environ['DJANGO_SETTINGS_MODULE'].replace('.settings', ''),
    },
}

CACHES = {
    'dummy': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    #'local': {
    #    'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    #},
}

BROKERS = {
    'local': {
        'TRANSPORT': 'amqp',
        'HOST':      'localhost',
        'PORT':      5672,
        'USER':      None,
        'PASSWORD':  None,
        'PATH':     'prism',
    },
}

################################################################################

if 'DATABASE_URL' in os.environ:
    link = urlparse(os.environ['DATABASE_URL'])

    DATABASES['heroku'] = {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     link.path[1:],
        'HOST':     link.hostname,
        'PORT':     link.port,
        'USER':     link.username,
        'PASSWORD': link.password,
    }

#*******************************************************************************

if 'CLEARDB_DATABASE_URL' in os.environ:
    link = urlparse(os.environ['CLEARDB_DATABASE_URL'])

    DATABASES['cleardb'] = {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     link.path[1:],
        'HOST':     link.hostname,
        'PORT':     link.port,
        'USER':     link.username,
        'PASSWORD': link.password,
    }

#*******************************************************************************

if 'MONGOLAB_URI' in os.environ:
    link = urlparse(os.environ['MONGOLAB_URI'])

    MONGO_DBs['mongolab'] = {
        'db':       link.path[1:],
        'host':     link.hostname,
        'port':     link.port,
        'username': link.username,
        'password': link.password,
    }

#*******************************************************************************

for alias,target in [
    ('cloud', "REDISCLOUD_URL"),
]:
    if target in os.environ:
        link = urlparse(os.environ[target])

        CACHES[alias] = {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': '%s:%d' % (link.hostname, link.port),
            'NAME':     link.path[1:],
            'OPTIONS':  {},
        }

        if len(link.password):
            CACHES[alias]['OPTIONS']['PASSWORD'] = link.password

        if len(link.path[1:]):
            CACHES[alias]['OPTIONS']['DB'] = int(link.path[1:])

#*******************************************************************************

for alias,target in [
    ('amqp', "CLOUDAMQP_URL"),
    ('mqtt', "CLOUDMQTT_URL"),
]:
    if target in os.environ:
        link = urlparse(os.environ[target])

        BROKERS[alias] = {
            'TRANSPORT': link.scheme,
            'HOST':      link.hostname,
            'PORT':      link.port or 5672,
            'USER':      link.username,
            'PASSWORD':  link.password,
            'PATH':     link.path[1:],
        }

################################################################################

#DATABASES['default'] = DATABASES.get('heroku', DATABASES.get('cleardb', DATABASES['local']))
DATABASES['default'] = DATABASES['local']

MONGO_DBs['default'] = MONGO_DBs.get('mongolab', MONGO_DBs['local'])

CACHES['default'] = CACHES.get('redis', CACHES.get('local', CACHES['dummy']))

BROKERS['default'] = BROKERS.get('amqp', BROKERS.get('mqtt', BROKERS['local']))

################################################################################

if 'SENDGRID_USERNAME' in os.environ:
    SMTP_SERVER   = 'smtp.sendgrid.net'
    SMTP_LOGIN    = os.environ.get('SENDGRID_USERNAME')
    SMTP_PASSWORD = os.environ.get('SENDGRID_PASSWORD')
    MAIL_SERVER   = 'smtp.sendgrid.net'
    MAIL_USERNAME = os.environ.get('SENDGRID_USERNAME')
    MAIL_PASSWORD = os.environ.get('SENDGRID_PASSWORD')
    MAIL_USE_TLS  = True
