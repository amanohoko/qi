from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/',           include(admin.site.urls)),

    url(r'^auth/email/?$',   'code2use.webapp.app.views.require_email', name='require_email'),
    url(r'^auth/logout/?$',  'code2use.webapp.app.views.logout', name='logout'),
    url(r'^auth/forgot/?$',  'code2use.webapp.app.views.validation_sent'),

    url(r'^auth/federated/(?P<backend>[^/]+)/$', 'code2use.webapp.app.views.ajax_auth', name='ajax-auth'),
    url(r'^auth/',           include('social.apps.django_app.urls', namespace='social')),

    url(r'^$',               'code2use.webapp.app.views.home', name='homepage'),

    url(r'^accounts$',             'code2use.webapp.app.views.accounts', name='done'),
    url(r'^profile/?$',            'code2use.webapp.app.views.profile_form', name='profile'),
    url(r'^profile/(?P<acte>.+)$', 'code2use.webapp.app.views.profile_form', name='profile'),
    url(r'^settings$',             'code2use.webapp.app.views.settings_form', name='settings'),
)
