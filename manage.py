#!/usr/bin/env python

import os, sys

import newrelic.agent

from django.core.management import execute_from_command_line

if __name__ == "__main__":
    #newrelic-admin run-program

    newrelic.agent.initialize

    #***************************************************************************

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "code2use.webapp.settings")

    #***************************************************************************

    execute_from_command_line(sys.argv)
