from setuptools import setup

setup(
    name='senility',
    version='0.0.1',
    description="'Senility for Deming",
    platforms=['Any'],
    packages=['code2use'],
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'autobahn.twisted.wamplet': [
            'backend = code2use.streams:AppSession'
        ],
    }
)
