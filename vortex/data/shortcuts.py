#-*- coding: utf-8 -*-

from code2use.shortcuts import *

#*******************************************************************************

from .models import *
from .forms  import *
from .tasks  import *

#*******************************************************************************
