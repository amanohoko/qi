#-*- coding: utf-8 -*-

from vortex.data.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'data/views/homepage.html'

    return {'version': version}

################################################################################

from . import custom

from . import ontologies

#*******************************************************************************

urlpatterns = fqdn.urlpatters
