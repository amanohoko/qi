#-*- coding: utf-8 -*-

from vortex.data.shortcuts import *

################################################################################

@fqdn.route(r'^tabular/$', strategy='login')
def table_home(context):
    context.template = 'data/rest/table/home.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^geo/$', strategy='login')
def geo_home(context):
    context.template = 'data/rest/geo/home.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^web/$', strategy='login')
def web_home(context):
    context.template = 'data/rest/web/home.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^graph/$', strategy='login')
def graph_home(context):
    context.template = 'data/rest/graph/home.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^ontologies/$', strategy='login')
def ontology_home(context):
    context.template = 'data/rest/ontology/home.html'

    return {'version': version}
