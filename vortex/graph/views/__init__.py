#-*- coding: utf-8 -*-

from vortex.graph.shortcuts import *

################################################################################

@fqdn.route(r'^$')
def homepage(context):
    context.template = 'graph/views/homepage.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^search$')
def search_box(context):
    context.template = 'graph/views/search.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^sparql$')
def sparql_cmd(context):
    context.template = 'graph/views/sparql.html'

    return {'version': version}

################################################################################

from . import ontologies

#*******************************************************************************

urlpatterns = fqdn.urlpatters
