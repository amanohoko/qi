#-*- coding: utf-8 -*-

from vortex.graph.shortcuts import *

#*******************************************************************************

from code2use.core.abstract import *

################################################################################

@fqdn.route(r'^ontologies/contacts/?$', strategy='login')
def contact_list(context):
    context.template = 'graph/rest/contact/listing.html'

    return {
        'listing': version,
    }

#*******************************************************************************

@fqdn.route(r'^ontologies/events/?$', strategy='login')
def event_list(context):
    context.template = 'graph/rest/event/listing.html'

    return {'version': version}
