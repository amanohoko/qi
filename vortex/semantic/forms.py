#-*- coding: utf-8 -*-

from .utils import *

from .models import *

################################################################################

class TokenizeForm(forms.Form):
    sentence = forms.CharField(max_length=256)

#*******************************************************************************
