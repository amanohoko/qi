#-*- coding: utf-8 -*-

from vortex.semantic.shortcuts import *

################################################################################

@fqdn.route(r'^nlp/tokenize$')
def tokenizer(context):
    context.template = 'semantic/views/nlp/tokenizer.html'

    resp = {
        'form': TokenizeForm(),
    }

    if context.method=='POST':
        resp['form'] = TokenizeForm(context.POST)

        if resp['form'].is_valid():
            resp['sentence'] = resp['form'].cleaned_data['sentence']

            resp['tokens'] = nltk.word_tokenize(resp['sentence'])

            resp['tagged'] = nltk.pos_tag(resp['tokens'])

            resp['entities'] = nltk.chunk.ne_chunk(resp['tagged'])

    return resp
