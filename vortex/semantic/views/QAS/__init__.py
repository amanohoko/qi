#-*- coding: utf-8 -*-

from vortex.semantic.shortcuts import *

################################################################################

@fqdn.route(r'^qa/understand$')
def nlp_sparql(context):
    context.template = 'semantic/views/qa/understand.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^qa/interpret$')
def rdf_nlp(context):
    context.template = 'semantic/views/qa/interpret.html'

    return {'version': version}
