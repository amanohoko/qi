#-*- coding: utf-8 -*-

from vortex.semantic.shortcuts import *

################################################################################

@fqdn.route(r'^$')
def homepage(context):
    context.template = 'semantic/views/homepage.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^rdf/enrich$')
def rdf_enrich(context):
    context.template = 'semantic/views/rdf/enrich.html'

    return {'version': version}

################################################################################

from . import NLP
from . import QAS

################################################################################

urlpatterns = fqdn.urlpatters
